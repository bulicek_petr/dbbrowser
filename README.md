# Build and run instructions

##Pre-requisites
- Java is installed
- Maven is installed

##Build
To build use following maven command, it produces runnable jar file
```sh
mvn clean package
```
##Run
To run use following command
```sh
java -jar <jar_file_created_by_maven>
```
## REST API Documentation

Swagger documentation is accessible when application is running on this url:
```sh  
http://<hostname>:8080/DbBrowser/swagger-ui.html
```

## DB

H2 in memory DB is used for application. It is possible to browse DB on this url
 when application is running:
```sh  
http://<hostname>:8080/DbBrowser/h2-console
```
