package com.ataccama.dbbrowser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.ataccama.dbbrowser.repository")
public class DbBrowserApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbBrowserApplication.class, args);
    }

}
