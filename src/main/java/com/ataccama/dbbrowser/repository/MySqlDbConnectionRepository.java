package com.ataccama.dbbrowser.repository;

import com.ataccama.dbbrowser.domain.MySqlDbConnection;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MySqlDbConnectionRepository extends JpaRepository<MySqlDbConnection, Long> {

	@Query(value = "SELECT * FROM MY_SQL_DB_CONNECTION LIMIT 5", nativeQuery = true)
	List<MySqlDbConnection> getDataPreview();

}
