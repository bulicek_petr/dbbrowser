package com.ataccama.dbbrowser.repository;

import com.ataccama.dbbrowser.domain.DbConnection;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StatsRepository extends JpaRepository<DbConnection, Long> {

	@Query(value = "SELECT c.TABLE_NAME as tableName, COUNT(c.COLUMN_NAME) as columnCount, t.ROW_COUNT_ESTIMATE as rowCount"
		+ " FROM INFORMATION_SCHEMA.COLUMNS c"
		+ " JOIN INFORMATION_SCHEMA.TABLES t ON t.TABLE_NAME = c.TABLE_NAME"
		+ " WHERE c.TABLE_SCHEMA <> 'INFORMATION_SCHEMA'",
		nativeQuery = true)
	List<TableStatsDto> getTableStats();

	interface TableStatsDto {
		String getTableName();
		Integer getColumnCount();
		Integer getRowCount();
	}

}
