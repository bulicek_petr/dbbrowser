package com.ataccama.dbbrowser.rest.controller;

import com.ataccama.dbbrowser.repository.StatsRepository.TableStatsDto;
import com.ataccama.dbbrowser.service.StatsService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stats")
public class StatsController {

	private StatsService statsService;

	public StatsController(StatsService statsService) {
		this.statsService = statsService;
	}

	@Operation(summary = "Returns stats of DB table")
	@GetMapping(value = "/table", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TableStatsDto>> getTableStats() {
		List<TableStatsDto> tablesStats = statsService.getTablesStats();
		return ResponseEntity.ok(tablesStats);
	}

}
