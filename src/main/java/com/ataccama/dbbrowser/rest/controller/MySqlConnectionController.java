package com.ataccama.dbbrowser.rest.controller;

import com.ataccama.dbbrowser.domain.MySqlDbConnection;
import com.ataccama.dbbrowser.service.impl.MySqlConnectionServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mySql")
public class MySqlConnectionController {

	private MySqlConnectionServiceImpl dbConnectionService;

	public MySqlConnectionController(MySqlConnectionServiceImpl dbConnectionService) {
		this.dbConnectionService = dbConnectionService;
	}

	@Operation(summary = "Creates new MySqlConnection entity")
	@PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MySqlDbConnection> createConnection(
			@Parameter(description = "JSON object of MySqlDbConnection entity", required = true)
			@RequestBody @Valid MySqlDbConnection mySqlDbConnection) {
		MySqlDbConnection connection = dbConnectionService.createConnection(mySqlDbConnection);
		return new ResponseEntity<>(connection, HttpStatus.CREATED);
	}

	@Operation(summary = "Returns all MySqlConnection entities")
	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<MySqlDbConnection>> getConnections() {
		List<MySqlDbConnection> connections = dbConnectionService.getConnections();
		return ResponseEntity.ok(connections);
	}

	@Operation(summary = "Updates new MySqlConnection entity")
	@PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MySqlDbConnection> updateConnections(
			@Parameter(description = "JSON object of MySqlDbConnection entity", required = true)
			@RequestBody @Valid MySqlDbConnection mySqlDbConnection) {
		MySqlDbConnection connection = dbConnectionService.updateConnection(mySqlDbConnection);
		return ResponseEntity.ok(connection);
	}

	@Operation(summary = "Returns MySqlConnection entity by id")
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MySqlDbConnection> getConnection(
			@Parameter(description = "ID of MySqlDbConnection entity", required = true)
			@PathVariable long id) {
		MySqlDbConnection connection = dbConnectionService.getConnection(id);
		return ResponseEntity.ok(connection);
	}

	@Operation(summary = "Deletes MySqlConnection entity by id")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<MySqlDbConnection> deleteConnection(
			@Parameter(description = "ID of MySqlDbConnection entity", required = true)
			@PathVariable long id) {
		dbConnectionService.deleteConnection(id);
		return ResponseEntity.noContent().build();
	}

	@Operation(summary = "Returns DB data preview")
	@GetMapping(value = "/preview", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<MySqlDbConnection>> getDataPreview() {
		final List<MySqlDbConnection> dataPreview = dbConnectionService.getDataPreview();
		return ResponseEntity.ok(dataPreview);
	}

}
