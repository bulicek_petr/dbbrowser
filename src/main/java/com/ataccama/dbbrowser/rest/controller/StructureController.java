package com.ataccama.dbbrowser.rest.controller;

import com.ataccama.dbbrowser.service.StructureService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotNull;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/structure")
public class StructureController {

	private StructureService structureService;

	public StructureController(StructureService structureService) {
		this.structureService = structureService;
	}

	@Operation(summary = "Returns DB schemas")
	@GetMapping(value = "/schema", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> getSchemas() {
		List<String> schemas = structureService.getSchemas();
		return ResponseEntity.ok(schemas);
	}

	@Operation(summary = "Returns DB tables")
	@GetMapping(value = "/table", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> getTables() {
		List<String> tables = structureService.getTables();
		return ResponseEntity.ok(tables);
	}

	@Operation(summary = "Returns DB columns with metadata")
	@GetMapping(value = "/column", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Map<String, String>>> getColumnsMetadata(
			@Parameter(description = "Table name which we want columns from", required = true)
			@NotNull @RequestParam String tableName) {
		Map<String, Map<String, String>> columnsMetadata = structureService.getColumnsMetadata(tableName);
		return ResponseEntity.ok(columnsMetadata);
	}

}
