package com.ataccama.dbbrowser.rest.dto;

import lombok.Data;

@Data
public class ColumnStatsDto {

	private String min;
	private String max;
	private String avg;
	private String median;

}
