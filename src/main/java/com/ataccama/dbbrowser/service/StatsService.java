package com.ataccama.dbbrowser.service;

import com.ataccama.dbbrowser.repository.StatsRepository.TableStatsDto;
import java.util.List;

/**
 * Service provides stats of database, like table stats
 */
public interface StatsService {

	List<TableStatsDto> getTablesStats();

}
