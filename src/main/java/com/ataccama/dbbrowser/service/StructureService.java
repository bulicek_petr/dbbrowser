package com.ataccama.dbbrowser.service;

import java.util.List;
import java.util.Map;

/**
 * Service provides structure and metadata information of the DB
 */
public interface StructureService {

	List<String> getSchemas();

	List<String> getTables();

	Map<String, Map<String, String>> getColumnsMetadata(String tableName);

}
