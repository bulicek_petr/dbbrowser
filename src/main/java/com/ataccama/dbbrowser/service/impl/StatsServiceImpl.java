package com.ataccama.dbbrowser.service.impl;

import com.ataccama.dbbrowser.repository.StatsRepository;
import com.ataccama.dbbrowser.repository.StatsRepository.TableStatsDto;
import com.ataccama.dbbrowser.service.StatsService;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class StatsServiceImpl implements StatsService {

	private StatsRepository statsRepository;

	public StatsServiceImpl(StatsRepository statsRepository) {
		this.statsRepository = statsRepository;
	}

	@Override
	public List<TableStatsDto> getTablesStats() {
		return statsRepository.getTableStats();
	}

}
