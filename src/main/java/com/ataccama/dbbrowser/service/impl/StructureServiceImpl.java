package com.ataccama.dbbrowser.service.impl;

import com.ataccama.dbbrowser.error.DbBrowserException;
import com.ataccama.dbbrowser.service.StructureService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import javax.validation.constraints.NotNull;
import org.springframework.stereotype.Service;

@Service
public class StructureServiceImpl implements StructureService {

	public static final String IS_PRIMARY_KEY = "IS_PRIMARY_KEY";
	public static final String COLUMN_NAME = "COLUMN_NAME";
	public static final String TABLE_SCHEMA = "TABLE_SCHEM";
	public static final String TABLE_NAME = "TABLE_NAME";
	public static final String TABLE = "TABLE";

	private DataSource dataSource;

	public StructureServiceImpl(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<String> getSchemas() {
		List<String> schemas = Lists.newArrayList();

		try (ResultSet schemasResultSet = dataSource.getConnection().getMetaData().getSchemas()) {
			while (schemasResultSet.next()) {
				schemas.add(schemasResultSet.getString(TABLE_SCHEMA));
			}

			return schemas;
		} catch (SQLException e) {
			throw new DbBrowserException("Error occurred during getting schemas.", e);
		}
	}

	@Override
	public List<String> getTables() {
		List<String> tables = Lists.newArrayList();

		try {
			DatabaseMetaData connectionMetaData = dataSource.getConnection().getMetaData();
			try (ResultSet tablesResultSet = connectionMetaData
					.getTables(null, null, null, new String[] {TABLE})) {
				while (tablesResultSet.next()) {
					String tableName = tablesResultSet.getString(TABLE_NAME);
					tables.add(tableName);
				}

				return tables;
			}
		} catch (SQLException e) {
			throw new DbBrowserException("Error occurred during getting tables.", e);
		}
	}

	@Override
	public Map<String, Map<String, String>> getColumnsMetadata(@NotNull String tableName) {
		Map<String, Map<String, String>> columnsMetadata = Maps.newHashMap();

		try {
			DatabaseMetaData connectionMetaData = dataSource.getConnection().getMetaData();
			try (ResultSet columnResultSet = connectionMetaData.getColumns(null, null, tableName.toUpperCase(), null)) {
				ResultSetMetaData columnResultSetMetadata = columnResultSet.getMetaData();

				while (columnResultSet.next()) {
					addColumnMetadata(columnsMetadata, columnResultSet, columnResultSetMetadata);
				}

				addColumnPrimaryKeysMetadata(columnsMetadata, tableName);
				return columnsMetadata;
			}
		} catch (SQLException e) {
			throw new DbBrowserException("Error occurred during getting column metadata.", e);
		}
	}

	private void addColumnMetadata(Map<String, Map<String, String>> columnMetadatas, ResultSet columnResultSet,
			ResultSetMetaData columnResultSetMetadata) throws SQLException {

		Map<String, String> columnMap = Maps.newHashMap();
		for (int i = 1; i <= columnResultSetMetadata.getColumnCount(); i++) {
			String attributeName = columnResultSetMetadata.getColumnName(i);
			String attributeLabel = columnResultSetMetadata.getColumnLabel(i);
			String attributeValue = columnResultSet.getString(attributeLabel);
			columnMap.put(attributeName, attributeValue);
		}
		String columnName = columnResultSet.getString(COLUMN_NAME);
		columnMap.put(IS_PRIMARY_KEY, "NO");
		columnMetadatas.put(columnName, columnMap);
	}

	private void addColumnPrimaryKeysMetadata(@NotNull Map<String, Map<String, String>> columnMetadatas,
			@NotNull String tableName) throws SQLException {

	    DatabaseMetaData connectionMetaData = dataSource.getConnection().getMetaData();
        ResultSet primaryKeysResultSet =
	        connectionMetaData.getPrimaryKeys(null, null, tableName.toUpperCase());

	    while (primaryKeysResultSet.next()) {
	        String primaryKeyColumnName = primaryKeysResultSet.getString(COLUMN_NAME);
	        columnMetadatas.get(primaryKeyColumnName).put(IS_PRIMARY_KEY, "YES");
	    }
	}

}
