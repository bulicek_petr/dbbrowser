package com.ataccama.dbbrowser.service.impl;

import com.ataccama.dbbrowser.domain.DbConnection;
import com.ataccama.dbbrowser.domain.MySqlDbConnection;
import com.ataccama.dbbrowser.error.NotFoundException;
import com.ataccama.dbbrowser.repository.MySqlDbConnectionRepository;
import com.ataccama.dbbrowser.service.DbConnectionService;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class MySqlConnectionServiceImpl implements DbConnectionService {

	private MySqlDbConnectionRepository dbConnectionRepository;

	public MySqlConnectionServiceImpl(MySqlDbConnectionRepository dbConnectionRepository) {
		this.dbConnectionRepository = dbConnectionRepository;
	}

	@Override
	public List<MySqlDbConnection> getConnections() {
		return dbConnectionRepository.findAll();
	}

	@Override
	public MySqlDbConnection getConnection(long id) {
		return dbConnectionRepository
			.findById(id)
			.orElseThrow(() -> getConnectionNotFoundException(id));
	}

	@Override
	public MySqlDbConnection createConnection(@NotNull DbConnection dbConnection) {
		dbConnection.setId(null);
		return dbConnectionRepository.save((MySqlDbConnection) dbConnection);
	}

	@Override
	public MySqlDbConnection updateConnection(@NotNull DbConnection dbConnection) {
		if (!dbConnectionRepository.existsById(dbConnection.getId())) {
            throw getConnectionNotFoundException(dbConnection.getId());
		}
		return dbConnectionRepository.save((MySqlDbConnection) dbConnection);
	}

	@Override
	public void deleteConnection(long id) {
		MySqlDbConnection mySqlDbConnection =
			dbConnectionRepository.findById(id).orElseThrow(() -> getConnectionNotFoundException(id));
		dbConnectionRepository.delete(mySqlDbConnection);
	}

	@Override
	public List<MySqlDbConnection> getDataPreview() {
		return dbConnectionRepository.getDataPreview();
	}

	private NotFoundException getConnectionNotFoundException(long id) {
		return new NotFoundException("MySqlDbConnection not found, id: " + id);
	}
}
