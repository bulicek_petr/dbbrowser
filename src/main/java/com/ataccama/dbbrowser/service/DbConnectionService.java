package com.ataccama.dbbrowser.service;

import com.ataccama.dbbrowser.domain.DbConnection;
import com.ataccama.dbbrowser.domain.MySqlDbConnection;
import java.util.List;
import javax.validation.constraints.NotNull;

/**
 * Service handles CRUD operations over DB connections
 */
public interface DbConnectionService {

	List<? extends DbConnection> getConnections();

	DbConnection getConnection(long id);

	DbConnection createConnection(@NotNull DbConnection dbConnection);

	DbConnection updateConnection(@NotNull DbConnection dbConnection);

	void deleteConnection(long id);

	List<MySqlDbConnection> getDataPreview();
}
