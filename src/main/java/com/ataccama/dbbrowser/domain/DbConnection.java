package com.ataccama.dbbrowser.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

@Data
@MappedSuperclass
public abstract class DbConnection {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "database_name", nullable = false)
    private String databaseName;

    @NotNull
    @Column(name = "host_name", nullable = false)
    private String hostName;

    @NotNull
    @Column(name = "port", nullable = false)
    private int port;

    @NotNull
    @Column(name = "user_name", nullable = false)
    private String userName;

    @NotNull
    @Column(name = "password", nullable = false)
    private String password;

}
