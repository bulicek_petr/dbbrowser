package com.ataccama.dbbrowser.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity(name = "MY_SQL_DB_CONNECTION")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class MySqlDbConnection extends DbConnection {

}
