package com.ataccama.dbbrowser.error;

import static org.springframework.http.ResponseEntity.status;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Log4j2
@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({NotFoundException.class})
	public ResponseEntity<ErrorResponse> handleNotFoundException(NotFoundException ex) {
		return handleException(ex, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({DbBrowserException.class})
	public ResponseEntity<ErrorResponse> handleDbBrowserException(DbBrowserException ex) {
		return handleException(ex, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		log.error(ex.getMessage());
		ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getMessage(), System.currentTimeMillis());
		return status(HttpStatus.BAD_REQUEST).body(errorResponse);
	}

	private ResponseEntity<ErrorResponse> handleException(Exception ex, HttpStatus httpStatus) {
		log.error(ex.getMessage(), ex);
		ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getMessage(), System.currentTimeMillis());
		return status(httpStatus).body(errorResponse);
	}

}
