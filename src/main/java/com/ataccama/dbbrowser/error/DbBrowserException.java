package com.ataccama.dbbrowser.error;

public class DbBrowserException extends RuntimeException {

	public DbBrowserException(String message, Throwable cause) {
		super(message, cause);
	}

}
